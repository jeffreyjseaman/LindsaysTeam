package student.first;

import student.main;

import javafx.scene.control.Button;
import java.io.IOException;

import javafx.fxml.FXML;


public class mainViewController {
	
	private main main;
	
	@FXML
	private void goHome() throws IOException{
		main.showMainItems();
		
		
	}

	@FXML
	private void close() throws IOException{
		main.closeProgram();
		
		
	}
	
	
	
	@FXML
	private void addBtn2() throws IOException{
		main.showAddStage2();
		
		
	}
	

}
