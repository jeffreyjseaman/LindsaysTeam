package student.first;


import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import student.main;



public class customerInfoController {
	
	


	private main main;

	
	@FXML
	private TextField fNameField;
	
	
	@FXML
	private TextField lNameField;
	
	@FXML
	private TextField addressField;
	
	@FXML
	private TextField cityField;
	
	@FXML
	private TextField stateField;
	
	@FXML
	private TextField zipField;
	
	@FXML
	private TextField phoneField;
	
	@FXML
	private TextField emailField;
	
	@FXML
	private Button saveButton;

	String fName, lName, address, city, state,zip, phone, email;
	

	
	void sendInfo(){
		fName = fNameField.getText();
		lName = lNameField.getText();
		address = addressField.getText();
		city = cityField.getText();
		state = stateField.getText();
		zip = zipField.getText();
		phone = phoneField.getText();
		email = emailField.getText();

	}
	
	
	@FXML
	private void sendInfo(ActionEvent event){
		try {
			FXMLLoader loader =  new FXMLLoader();
			
			Parent parent = FXMLLoader.load(getClass().getResource("first/lastPageController.fxml"));
			
			lastPageController out = new lastPageController();
			
			out = loader.getController();
			out.setSelect (fName, lName, address, city, state,zip, phone, email);
			
			Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			stage.setScene(new Scene (parent));
			stage.show();
		} catch (IOException ex){
			
			Logger.getLogger(customerInfoController.class.getName()).log(Level.SEVERE, null, ex);
		}
			
	}
	
	
	@FXML
	private void close() throws IOException{
		main.closeProgram();
		
		
	}

	


}
