package student.first;


import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import student.main;
import student.first.customerInfoController;



public class lastPageController {
	
	private main main;

	
	@FXML
	private TextField fNameField;
	
	
	@FXML
	private TextField lNameField;
	
	@FXML
	private TextField addressField;
	
	@FXML
	private TextField cityField;
	
	@FXML
	private TextField stateField;
	
	@FXML
	private TextField zipField;
	
	@FXML
	private TextField phoneField;
	
	@FXML
	private TextField emailField;
	
	@FXML
	private Button saveButton;

	

	
	
	public void setSelect(String fName, String lName, String address, String city, String state,String zip, String phone,String email ){
		
		fNameField.setText(fName);
		lNameField.setText(lName);
		addressField.setText(address);
		cityField.setText(city);
		stateField.setText(state);
		zipField.setText(zip);
		phoneField.setText(phone);
		emailField.setText(email);
		
		
	}
	
	
	
	
	
	@FXML
	private void close() throws IOException{
		main.closeProgram();
		
		
	}

	


}
