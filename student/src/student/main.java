
package student;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class main extends Application {
	static Stage window;
	static Stage primaryStage;
	static BorderPane mainlayout;
	
	
	@Override
	public void start(Stage primaryStage) throws IOException {
		
		window = primaryStage;
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Car Rental");
		
		showMainView();
		showMainItems();
	}
	
	public void showMainView() throws IOException{
		
		FXMLLoader myloader = new FXMLLoader();
		myloader.setLocation(main.class.getResource("first/MainView.fxml"));
		mainlayout = myloader.load();
		Scene myscene = new Scene (mainlayout);
		primaryStage.setScene(myscene);
		primaryStage.show();
		 
		 
	}


	public static void showMainItems() throws IOException{
		
		FXMLLoader myloader = new FXMLLoader();
		myloader.setLocation(main.class.getResource("first/MainItems.fxml"));
		BorderPane mainItems = myloader.load();
		mainlayout.setCenter(mainItems);
		 
		 
	}
	
	public static void show1stScene() throws IOException{
		FXMLLoader myloader = new FXMLLoader();
		myloader.setLocation(main.class.getResource("first/FirstPage.fxml"));
		BorderPane customerInfo = myloader.load();
		mainlayout.setCenter(customerInfo);

		
	}
	public static void show2ndScene() throws IOException{
		FXMLLoader myloader = new FXMLLoader();
		myloader.setLocation(main.class.getResource("first/customerInfo.fxml"));
		BorderPane SecondPage = myloader.load();
		mainlayout.setCenter(SecondPage);

		
	}
	

	
	
	
	public static void showAddStage2() throws IOException{
	
		FXMLLoader myloader = new FXMLLoader();
		myloader.setLocation(main.class.getResource("first/FirstPage.fxml"));
		BorderPane SecondPage = myloader.load();
		mainlayout.setCenter(SecondPage);

		
	}
		
		
		

	
	public static void closeProgram() throws IOException{

			window.close();
	}
	

	public static void main(String[] args) {
		launch(args);
	}
}
